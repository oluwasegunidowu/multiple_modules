provider "aws" {
  region = var.region
  #profile = "default"
}

terraform {
  backend "s3" {
    bucket = "2tierarchtestbucket"
    region = "us-east-2"
    key    = "dev/terraform.tfstate"
    #profile = "default"
  }
  }