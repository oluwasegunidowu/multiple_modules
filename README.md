# WORKING WITH MULTIPLE MODULES

## Prerequisites
- A server or local machine that has Terraform installed in it & AWS CLI also!
- AWS Access Key & Secret Key

## Description
Here I used terraform modules in differnt environments;

- stg = Test environment
- dev = Development environment
- prod = production environment 

to showcase multiple modules and maintain different infrastructure for different environment

No value was used for the variables, so that different values can be used for different environments.

## s3 Bucket
S3 bucket "2tierarchtestbucket" is used to store the state files instead of storing it locally
- Note -> the s3 bucket should be created in AWS prior

## How to use
Run the commands below one after the other in each folder i.e. stg, dev & prod and you will see 3 different instances & for that 3 different VPCs and state file for each environment.

- terraform init
- terraform plan
- terraform apply -auto-approve