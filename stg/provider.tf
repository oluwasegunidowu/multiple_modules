provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    bucket = "2tierarchtestbucket"
    region = "us-east-2"
    key    = "stg/terraform.tfstate"
    profile = "default"
  }
}